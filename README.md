# Respaldar tema do foro CnG

script para respaldar un tema do foro

Crea un único ficheiro **.html** para visualizar o tema completo.

## Exemplo

ficheiro creado co script (**v0.1**) dun tema de dúas páxinas

[Exemplo][1]


## Require

- wget
- permisos para executar scripts

### parámetros

- URL completa da **páxina incial** do tema a descargar
- número total de páxinas do tema

## executar

- facelo executable e chutalo

    chmod +x respaldo.sh

    ./respaldo
    
- preguntarache pola URL do fío a descargar e o número de páxinas (vese no foro).

crea un ficheiro con nome **fio_cng_completo.html** no mesmo directorio que o executable que podes visualizar con calquera navegador.

## Versión

- 0.1 commit inicial

    funcional, con parámetros básicos para a descarga do tema completo de www.correrengalicia.org

    
## Contribúe

O script ten licenza libre (CC SA) polo que es libre de facer con el o que queiras (compartindo igual)

Podes clonar o repositorio e aportar melloras e probalo en diferentes SO.

Abre un _issue_ con un problema o suxestión

## ToDo

- editar o ficheiro resultante desde o script (awk, sed, ...)
    - eliminar partes repetidas
    - eliminar referencias a scripts non necesarios

- configurar comportamento se escribes URL incorrecto
- configurar comportamento se escribes un número incorrecto (superior) de páxinas
- permitir escoller rango de páxinas
- permitir facer varios ficheiros en lugar de un só (para ficheiros moi grandes?)

## Contacto

[XoseM][2] ✉️GPG:[BCEED050][3]



[1]: http://xmgz.eu/test_script_cng/test.html
[2]: http://xmgz.eu/#5
[3]: http://keys.gnupg.net/pks/lookup?op=get&search=0xFE270ED0BCEED050
