#!/bin/bash
# script para descargar un tema completo do foro

##       www.correrengalicia.org

## versión: 0.1
## por XoseM (xmgz.eu)
## licenza: CC SA

#######################################

##           recollendo datos   #######



## Preguntache por

# enderezo URL do tema
# Hai que indicar o enderezo da primeira páxina do tema
# tal como aparece no navegador.
# O parámetro start=0 é o que indica que comece pola primeira
# e o motor do foro divide os temas en páxinas de 15 comentarios.

echo "dirección URL do tema?"
read urlcng


echo "cantas páxinas ten o tema?"
## aquí poderíase facer que descargase un intervalo concreto de páxinas
## ao prezo de complicar o script. Por agora ben vale.
## A intención é facer unha copia de seguridade do tema, asi que
## descargamolo enteiro.
read k

paxina=1
parametro=0
urlcng=$urlcng"&start="

echo total de paxinas $k


while [ $paxina -le $k ]

## para que vaia escribindo a saída e depurar mellor
do
	echo \#páxina $paxina con url  $urlcng$parametro
	echo descargando...
	wget -np -c -O tema_cng_$paxina.html $urlcng$parametro
((parametro=parametro+15))
((paxina++))

done


## en este momento temos tantos ficheiros como páxinas
## ten o tema no foro

############################################

###########      editando os datos     #####

## igual, para que se vexa o que fai...
echo xuntando as páxinas
cat tema_cng_* >> fio_cng_completo.html

#unha vez creado o ficheiro eliminamos as "partes"
echo eliminando paxinas
rm tema_cng_*

### indicamoslle ao usuario o ficheiro resultante que debe abrir
echo Feito! abre no teu navegador o ficheiro fio_cng_completo.html
## como non sei o navegador nin o sistema operativo do usuario omito
## unha orde para abrir automáticamente o ficheiro.
## Pero, en linux,  sería algo tipo 

##   DESCOMENTA A SEGUINTE LIÑA PARA QUE SE EXECUTE AUTOMÁTICAMENTE 

## firefox `pwd`/ficheiro_cng_completo.html &


